package com.service;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "rates",
        "base",
        "date"
})
public class Query {

    @JsonProperty("rates")
    private Rates rates;
    @JsonProperty("base")
    private String base;
    @JsonProperty("date")
    private String date;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private Map<String, Object> ratesMap = new HashMap<String, Object>();


    @JsonProperty("rates")
    public Rates getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(Rates rates) {
        this.rates = rates;
    }

    @JsonProperty("base")
    public String getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(String base) {
        this.base = base;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Query{" +
                "rates=" + rates +
                ", base='" + base + '\'' +
                ", date='" + date + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public Map<String, Object> getRatesMap() {
        ratesMap.put("base", this.getBase());
        ratesMap.put("CAD", rates.getCAD());
        ratesMap.put("HKD", rates.getHKD());
        ratesMap.put("PHP", rates.getPHP());
        ratesMap.put("DKK", rates.getDKK());
        ratesMap.put("HUF", rates.getHUF());
        ratesMap.put("CZK", rates.getCZK());
        ratesMap.put("AUD", rates.getAUD());
        ratesMap.put("RON", rates.getRON());
        ratesMap.put("SEK", rates.getSEK());
        ratesMap.put("IDR", rates.getIDR());
        ratesMap.put("INR", rates.getINR());
        ratesMap.put("BRL", rates.getBRL());
        ratesMap.put("RUB", rates.getRUB());
        ratesMap.put("HRK", rates.getHRK());
        ratesMap.put("JPY", rates.getJPY());
        ratesMap.put("THB", rates.getTHB());
        ratesMap.put("CHF", rates.getCHF());
        ratesMap.put("SGD", rates.getSGD());
        ratesMap.put("PLN", rates.getPLN());
        ratesMap.put("BGN", rates.getBGN());
        ratesMap.put("TRY", rates.getTRY());
        ratesMap.put("CNY", rates.getCNY());
        ratesMap.put("NOK", rates.getNOK());
        ratesMap.put("NZD", rates.getNZD());
        ratesMap.put("ZAR", rates.getZAR());
        ratesMap.put("USD", rates.getUSD());
        ratesMap.put("MXN", rates.getMXN());
        ratesMap.put("ILS", rates.getILS());
        ratesMap.put("GBP", rates.getGBP());
        ratesMap.put("KRW", rates.getKRW());
        ratesMap.put("MYR", rates.getMYR());
        return ratesMap;
    }
}

