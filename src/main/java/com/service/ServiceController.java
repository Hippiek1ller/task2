package com.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Controller
public class ServiceController {

    @GetMapping
    public String main(Map<String, Object> model) {
        RestTemplate restTemplate = new RestTemplate();
        Query query = restTemplate.getForObject("https://api.exchangeratesapi.io/latest", Query.class);
        model.putAll(query.getRatesMap());
        return "main";
    }

    @PostMapping
    public String result(@RequestParam String from, @RequestParam String to, @RequestParam String amount, Map<String, Object> model) {
        RestTemplate restTemplate = new RestTemplate();
        ExchangeService exchangeService = new ExchangeService();
        Query query = restTemplate.getForObject("https://api.exchangeratesapi.io/latest?base=" + from, Query.class);
        Double rate = (Double) query.getRatesMap().get(to);
        Double resultOfExchange = exchangeService.changeCurrency(Double.parseDouble(amount), rate);
        model.put("from", from);
        model.put("to", to);
        model.put("amount", amount);
        model.put("result", resultOfExchange);
        return "result";
    }
}
