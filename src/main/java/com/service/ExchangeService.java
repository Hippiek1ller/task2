package com.service;

public class ExchangeService {

    public double changeCurrency(double currencyAmount, double exchangeRate) {
        return currencyAmount * exchangeRate;
    }
}
